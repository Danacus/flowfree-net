﻿Option Strict On

Public Class MainForm
    Public Shared maxGridSize As Integer = 24
    Public Shared levelGenerators() As ILevelGenerator = {New DeadEnd, New FlowGrower, New FlowShrinker}

    Private Sub StartButton_Click(sender As Object, e As EventArgs) Handles StartButton.Click
        Dim gameForm As GameForm = New GameForm
        gameForm.gridSize = GridSizeBox.SelectedIndex + 4
        gameForm.generator = levelGenerators(GeneratorBox.SelectedIndex)
        gameForm.enablePreview = PreviewBox.Checked
        gameForm.main = Me
        gameForm.Show()
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim rand = New Random
        ImageLoader.Load()

        For i As Integer = 4 To maxGridSize
            GridSizeBox.Items.Add(i + 1 & " x " & i + 1)
        Next

        GridSizeBox.SelectedItem = "5 x 5"

        For i As Integer = 0 To levelGenerators.GetUpperBound(0)
            GeneratorBox.Items.Add(levelGenerators(i).name)
        Next

        GeneratorBox.SelectedIndex = 0
    End Sub
End Class
