﻿Option Strict On

Public Class GameForm
    Public gridSize As Integer
    Public grid As Grid
    Public solver As Solver
    Public main As MainForm
    Public generator As ILevelGenerator
    Public enablePreview As Boolean

    Dim stopwatch As New Stopwatch

    Public Shared prevWindowState As FormWindowState
    Public Shared prevSize As Size

    Private Sub GameForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        WindowState = FormWindowState.Maximized

        grid = New Grid(Me)
        Controls.Add(grid)

        LevelGenWorker.RunWorkerAsync()
    End Sub

    Public Sub EndGame()
        Dim message As String = ""

        Dim currentTime = stopwatch.Elapsed
        stopwatch.Stop()

        If GameTimer.RegisterTime(gridSize, currentTime) Then
            message = "Congratulations! Level Completed!" & Environment.NewLine & "New highscore: " & GetTimeString(GameTimer.GetBestTime(gridSize)) & Environment.NewLine & "Would you like to play a new game?"
        Else
            message = "Congratulations! Level Completed!" & Environment.NewLine & "Time: " & GetTimeString(currentTime) & Environment.NewLine & "Would you like to play a new game?"
        End If

        Dim Result = MessageBox.Show(Me, message, "Good job!", MessageBoxButtons.YesNo)

        If Result = DialogResult.Yes Then
            Close()
        End If
    End Sub

    Sub UpdateTimeLabel(time As TimeSpan)
        currentTimeLabel.Text = "Current time: " & GetTimeString(time)
    End Sub

    ' I made a bit of a mess when trying to reduce the amount of redraws, but it works just fine
    Private Sub GameForm_ResizeEnd(sender As Object, e As EventArgs) Handles MyBase.ResizeEnd
        ' Only redraw the grid when we're sure the grid exists
        If grid IsNot Nothing And Not prevSize = Size Then
            grid.DrawGrid()
            prevSize = Size
        End If
    End Sub

    Private Sub GameForm_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        If Not WindowState = prevWindowState Then
            prevWindowState = WindowState
            GameForm_ResizeEnd(sender, e)
        End If
    End Sub

    Private Sub UITimer_Tick(sender As Object, e As EventArgs) Handles UITimer.Tick
        UpdateTimeLabel(stopwatch.Elapsed)
    End Sub

    Function GetTimeString(timeSpan As TimeSpan) As String
        Return New TimeSpan(timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, CInt(Math.Round(timeSpan.Milliseconds / 10, 2))).ToString()
    End Function

    Private Sub LevelGenWorker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles LevelGenWorker.DoWork
        generator.GenerateLevel(grid, LevelGenWorker)
        If LevelGenWorker.CancellationPending Then
            e.Cancel = True
        End If
    End Sub

    Private Sub LevelGenWorker_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles LevelGenWorker.RunWorkerCompleted
        solver = New Solver(Me)

        If Not enablePreview Then
            grid.CleanupFlows()
            stopwatch.Start()
        Else
            For i As Integer = 0 To grid.flows.GetUpperBound(0)
                grid.flows(i).SetTilesComplete(True)
            Next

            solver.enabled = False
        End If

        prevWindowState = WindowState
        prevSize = Size
        grid.DrawGrid()

        If GameTimer.bestTimes.ContainsKey(gridSize) Then
            BestTimeLabel.Text = "Best time: " & GetTimeString(GameTimer.GetBestTime(gridSize))
        Else
            BestTimeLabel.Text = ""
        End If

        LevelGenLabel.Visible = False
    End Sub

    Private Sub LevelGenWorker_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles LevelGenWorker.ProgressChanged
        LevelGenLabel.Text = "Generating Level... Iterations: " & e.ProgressPercentage.ToString()
    End Sub

    Private Sub GameForm_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        LevelGenWorker.CancelAsync()
    End Sub
End Class