﻿# FlowFree.NET

Daan Vanoverloop

## Algoritmes

De volgende algoritmes heb ik geïmplemeteerd in mijn spel:

- FlowShrinker
  - Bron: https://stackoverflow.com/a/12926928
  - Start met een geldige configuratie. Krimp de flows en zorg er tegelijkertijd voor dat een andere kop of staart de lege plek kan vullen.
  - Snel, maar levels zijn eenvoudig en minder interessant
  - Kan zelfs bij zeer grote roosters een geldig level genereren

- FlowGrower
  - Bron: https://p.cygnus.cc.kuleuven.be/bbcswebdav/pid-24096861-dt-content-rid-186606220_2/xid-186606220_2
  - Start met willekeurige koppels op het bord en groei de flows.
  - Trager, maar redelijk geode resultaten
  - Max. 20x20

- DeadEnd
  - Bron: ik
  - Start bij een kleurloos vakje en blijf die flow zo lang mogelijk groeien of tot dat een maximum lengte beriekt wordt.
  - Bij keuze van start van een flow, probeer je steeds een "doodlopend punt" te kiezen, dat is een kleurloos vakje met slechts 1 kleurloze buur
  - Zeer snel met zeer goede, moeilijke levels als resultaat
  - Max. 20x20

## Terminologie

- Tile: een tegel op het spelbord.
- Flow: een lijst van tegels die met elkaar "verbonden" zijn.
- Head: het eerste element van een flow.
- Tail: het laatste element van een flow, niet noodzakelijk een eindpunt ("node").
- Node: een punt dat steeds op dezelfde plaats staat en dat bij het oplossen van de puzzel verbonden moet worden met de overeenkomstige node van dezelfde kleur.
- Een flow is volledig wanneer het de 2 nodes van de kleur van de flow bevat.
- Een grid is volledig wanneer alle vakjes gekleurd zijn en wanneer alle flows volledig zijn.