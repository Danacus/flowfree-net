﻿Public Interface ILevelGenerator
    Property name As String
    Sub GenerateLevel(grid As Grid, worker As System.ComponentModel.BackgroundWorker)
End Interface