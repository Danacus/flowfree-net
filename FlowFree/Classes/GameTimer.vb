﻿Option Strict On

Public Class GameTimer
    Public Shared bestTimes As New Dictionary(Of Integer, TimeSpan)

    Public Shared Function RegisterTime(groupId As Integer, time As TimeSpan) As Boolean
        If Not bestTimes.ContainsKey(groupId) Then
            bestTimes(groupId) = time
            Return True
        End If

        If time.CompareTo(bestTimes.Item(groupId)) < 0 Then
            bestTimes(groupId) = time
            Return True
        End If

        Return False
    End Function

    Public Shared Function GetBestTime(groupId As Integer) As TimeSpan
        Return bestTimes(groupId)
    End Function
End Class
