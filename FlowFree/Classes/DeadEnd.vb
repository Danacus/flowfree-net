﻿Option Strict On

Public Class DeadEnd
    Implements ILevelGenerator

    Dim _name As String = "DeadEnd (fast and powerful, best results)"

    Public Property name As String Implements ILevelGenerator.name
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    Public Sub GenerateLevel(grid As Grid, worker As System.ComponentModel.BackgroundWorker) Implements ILevelGenerator.GenerateLevel
        ' **********************
        ' | Dead End Algortihm |
        ' **********************
        ' - By me!
        ' **********************

        ' 1. Pick a random black tile, preferably one with 1 black neighbour (a "dead end")
        ' 2. Keep adding new adjacent tiles to the flow for a certain amount of times (can be randomised)
        '    Can be modified to prevent creation of "dead tiles" (a black tile with no black neighbours)
        ' 3. If the grid is full, meaning each tile is colored, we have created a valid level
        '    If there is a dead tile in the grid, clear the grid and go to 1
        '    Otherwise, go to 1

        ' We will use a counter to determine if the grid is full, for performance reasons
        Dim counter = 0

        Dim g = grid.grid

        ' We need to use a dynamic data structure for our flows
        ' So we will be using a list and convert it to an array at the end
        Dim flows = New List(Of Flow)

        Dim gridSize = g.GetLength(0)

        Dim rnd = New Random

        Dim attempts As Integer = 0

        While counter < Math.Pow(gridSize, 2)
            ' Step 1

            Dim deadEnds = GetDeadEnds(grid)
            Dim tile As Tile
            Dim f As New Flow

            If deadEnds.Count > 0 Then
                ' Try to pick a dead end
                tile = deadEnds(rnd.Next(deadEnds.Count))
            Else
                ' Otherwise, pick a random black tile
                While True
                    Dim i = rnd.Next(gridSize - 1)
                    Dim j = rnd.Next(gridSize - 1)

                    If g(i, j).colorGroup = -1 Then
                        tile = g(i, j)
                        Exit While
                    End If
                End While
            End If

            ' Add the tile to the flow
            tile.colorGroup = flows.Count
            f.path.Add(tile)
            counter += 1

            ' Step 2

            ' We use the condition that a flow must be as large as possible, but smaller than 1.5 times the grid size
            While f.path.Count < gridSize * 2
                ' The expression for strict mode might be confusing, so let me clarify:
                ' The so called strict mode simply says that a flow is not allowed to "touch itself"
                ' This way the levels will be more interesting
                ' 1. Grids smaller then 13 x 13 will always try to use strict mode
                ' 2. Larger grids use dynamic strict mode, which automatically disables strict mode when a certain amount of the grid is filled,
                ' depending on the amount of attempts, this way it will find some kind of "sweet spot"

                Dim neighbours = GetValidNeighbours(grid, tile, gridSize < 14 Or counter < Math.Pow(gridSize, 2) / (attempts / 1000))

                Dim deadNeighbours = neighbours.Where(Function(n) grid.GetNeighbours(n).Where(Function(t) t.colorGroup = -1).Count = 0)
                Dim deadEndNeighbours = neighbours.Where(Function(n) grid.GetNeighbours(n).Where(Function(t) t.colorGroup = -1).Count = 1)

                If neighbours.Count > 0 Then
                    ' For as long as we have neighbours we can add a random one
                    ' or pick a dead or dead end neighbour if possible
                    ' This increases the overall performance and speed of the algorithm
                    If deadNeighbours.Count > 0 Then
                        tile = deadNeighbours(rnd.Next(deadNeighbours.Count))
                    ElseIf deadEndNeighbours.Count > 0 Then
                        tile = deadEndNeighbours(rnd.Next(deadEndNeighbours.Count))
                    Else
                        tile = neighbours(rnd.Next(neighbours.Count))
                    End If

                    tile.colorGroup = flows.Count
                    f.AddTile(tile)
                    counter += 1
                Else
                    ' Otherwise we will exit the loop
                    Exit While
                End If
            End While

            ' Now we can add the new flow
            flows.Add(f)

            'Step 3

            If GetDeadTiles(grid).Count > 0 Or f.path.Count < 1 Then
                ' If the last flow is invalid, or there is a dead tile, 
                ' clear the flows and the grid and reset the counter
                flows.Clear()
                grid.Clear()
                counter = 0
                attempts += 1

                ' Here I'm trying to reduce the amount of calls to the GameForm
                If (attempts Mod 50 = 0) Then
                    worker.ReportProgress(attempts)
                End If

                If worker.CancellationPending Then
                    Return
                End If
            End If
        End While

        ' I'm not quite sure why I have to force them in the correct state, 
        ' but it doesn't hurt doing so

        For Each f As Flow In flows
            For Each tile In f.path
                tile.isNode = False
            Next
            f.GetHead().isNode = True
            f.GetTail().isNode = True
        Next

        ' I don't think I have to ReDim, but I'm doing it anyway
        ReDim grid.flows(flows.Count - 1)
        grid.flows = flows.ToArray()
    End Sub

    ' The name of this function doesn't make a lot of sense, but I didn't want to write the same thing twice
    Function GetDead(grid As Grid, n As Integer) As List(Of Tile)
        Dim l = New List(Of Tile)

        For Each tile As Tile In grid.grid
            If (tile.colorGroup = -1 And grid.GetNeighbours(tile).Where(Function(t) t.colorGroup = -1).Count = n) Then
                l.Add(tile)
            End If
        Next

        Return l
    End Function

    Function GetDeadEnds(grid As Grid) As List(Of Tile)
        Return GetDead(grid, 1)
    End Function

    Function GetDeadTiles(grid As Grid) As List(Of Tile)
        Return GetDead(grid, 0)
    End Function

    Function GetValidNeighbours(grid As Grid, tile As Tile, strictMode As Boolean) As IEnumerable(Of Tile)
        If strictMode Then
            ' If we are in the so called "strict mode", we will not allow a flow to "touch" itself
            ' The original game usually does this by default, but with this algorithm, it won't work for larger grids
            Return grid.GetNeighbours(tile).Where(Function(n) n.colorGroup = -1 And grid.GetNeighbours(n).Where(Function(t) t.colorGroup = tile.colorGroup).Count = 1)
        Else
            Return grid.GetNeighbours(tile).Where(Function(n) n.colorGroup = -1)
        End If
    End Function
End Class
