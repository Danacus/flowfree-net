﻿Option Strict On

Public Class FlowGrower
    Implements ILevelGenerator

    Public _name As String = "FlowGrower (slower, decent results)"

    Private Property name As String Implements ILevelGenerator.name
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    ' Source of the algorithm: https://p.cygnus.cc.kuleuven.be/bbcswebdav/pid-24096861-dt-content-rid-186606220_2/xid-186606220_2
    Public Sub GenerateLevel(grid As Grid, worker As System.ComponentModel.BackgroundWorker) Implements ILevelGenerator.GenerateLevel
        Dim isDone = False
        Dim counter = 1

        ' So I don't have to write grid.grid over and over again
        Dim g = grid.grid

        Dim gridSize = g.GetLength(0)

        Dim rand As New Random

        While Not isDone
            counter += 1

            ' We can use make this dependent on the number of attempts
            ReDim grid.flows(CInt(counter / 100))

            ' A shorthand for grid.flows
            Dim flows = grid.flows

            Dim filled = 0


            ' Here I'm trying to reduce the amount of calls to the GameForm
            If (counter Mod 50 = 0) Then
                worker.ReportProgress(counter)
            End If

            If worker.CancellationPending Then
                Return
            End If

            ' Part 1: Initial state

            For i As Integer = flows.GetLowerBound(0) To flows.GetUpperBound(0)
                Dim fl As Flow = New Flow

                Dim first As Tile
                Dim neighbours As IEnumerable(Of Tile)

                ' Start by finding 2 adjacent tiles
                Do
                    first = g(rand.Next(g.GetUpperBound(0)), rand.Next(g.GetUpperBound(1)))
                    neighbours = grid.GetNeighbours(first).Where(Function(n) n.colorGroup = -1)
                Loop Until first.colorGroup = -1 And neighbours.Count > 0

                Dim second = neighbours(rand.Next(neighbours.Count))

                first.colorGroup = i
                second.colorGroup = i

                ' These are the nodes
                first.isNode = True
                second.isNode = True

                ' Add them to the flow
                fl.path.Add(first)
                fl.path.Add(second)

                ' Count the amount of tiles we have colored on the grid
                filled += 2

                flows(i) = fl
            Next

            ' Part 2

            Dim flowIndexes As New List(Of Integer)
            Dim indexes As List(Of Integer)
            Dim r As Integer
            Dim f As Flow
            Dim headNeighbours As IEnumerable(Of Tile)
            Dim tailNeighbours As IEnumerable(Of Tile)
            Dim rint As Integer
            Dim newTile As Tile

            For i As Integer = 0 To flows.GetUpperBound(0)
                ' Create a list of integers form 0 to the amount of flows
                flowIndexes.Add(i)
            Next

            ' While the grid is not filled ...
            While filled < Math.Pow(gridSize, 2)
                ' Copy the flowIndexes into another list
                indexes = New List(Of Integer)(flowIndexes)

                ' While there are still indexes in de list ...
                While indexes.Count > 0
                    ' Pick a random flow
                    r = indexes(rand.Next(indexes.Count - 1))
                    f = flows(r)

                    ' Remove that index
                    indexes.Remove(r)

                    ' Try to find neighbours of the head and tail
                    headNeighbours = GetValidNeighbours(grid, f.GetHead(), gridSize < 10)
                    tailNeighbours = GetValidNeighbours(grid, f.GetTail(), gridSize < 10)

                    If tailNeighbours.Count = 0 And headNeighbours.Count = 0 Then
                        ' If we can't find any valid neighbours, continue with the next iteration
                        ' (pick another random flow)
                        isDone = False
                        Continue While
                    End If

                    ' Pick a random neighbour
                    rint = rand.Next(headNeighbours.Count + tailNeighbours.Count)

                    ' Add a new tile to the flow
                    If rint < headNeighbours.Count Then
                        newTile = headNeighbours(rint)
                        newTile.isNode = True
                        newTile.colorGroup = f.GetHead().colorGroup
                        f.GetHead().isNode = False
                        f.path.Insert(0, newTile)
                    Else
                        newTile = tailNeighbours(rint - headNeighbours.Count)
                        newTile.isNode = True
                        newTile.colorGroup = f.GetTail().colorGroup
                        f.GetTail().isNode = False
                        f.path.Add(newTile)
                    End If

                    filled += 1

                    ' Exit the while and allow the main loop to exit (if the grid is filled)
                    isDone = True
                    Exit While
                End While

                If Not isDone Then
                    ' If isDone is False, it means that we cannot expand any flow,
                    ' so we will have to restart the algorithm by clearing the grid and exiting the current loop
                    grid.Clear()
                    Exit While
                End If
            End While
        End While
    End Sub

    Function GetValidNeighbours(grid As Grid, tile As Tile, strictMode As Boolean) As IEnumerable(Of Tile)
        If strictMode Then
            ' If we are in the so called "strict mode", we will not allow a flow to "touch" itself
            ' The original game usually does this by default, but with this algorithm, it won't work for larger grids
            Return grid.GetNeighbours(tile).Where(Function(n) n.colorGroup = -1 And grid.GetNeighbours(n).Where(Function(t) t.colorGroup = tile.colorGroup).Count = 1)
        Else
            Return grid.GetNeighbours(tile).Where(Function(n) n.colorGroup = -1)
        End If
    End Function
End Class
