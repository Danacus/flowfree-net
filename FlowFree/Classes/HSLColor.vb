﻿' ***************************************
' HSLColor Class by Richard Newman
' Source: https://richnewman.wordpress.com/about/code-listings-and-diagrams/hslcolor-class/
' ***************************************

' Converted with: https://www.carlosag.net/tools/codetranslator/

Public Class HSLColor

    ' Private data members below are on scale 0-1
    ' They are scaled for use externally based on scale
    Private _hue As Double = 1

    Private _saturation As Double = 1

    Private _luminosity As Double = 1

    Private Const scale As Double = 255

    Public Property Hue As Double
        Get
            Return (Me._hue * scale)
        End Get
        Set
            Me._hue = Me.CheckRange((Value / scale))
        End Set
    End Property

    Public Property Saturation As Double
        Get
            Return (Me._saturation * scale)
        End Get
        Set
            Me._saturation = Me.CheckRange((Value / scale))
        End Set
    End Property

    Public Property Luminosity As Double
        Get
            Return (Me._luminosity * scale)
        End Get
        Set
            Me._luminosity = Me.CheckRange((Value / scale))
        End Set
    End Property

    Private Function CheckRange(ByVal value As Double) As Double
        If (value < 0) Then
            value = 0
        ElseIf (value > 1) Then
            value = 1
        End If

        Return value
    End Function

    Public Overrides Function ToString() As String
        Return String.Format("H: {0:#0.##} S: {1:#0.##} L: {2:#0.##}", Me._hue, Me._saturation, Me._luminosity)
    End Function

    Public Function ToRGBString() As String
        Dim color As Color = CType(Me, Color)
        Return String.Format("R: {0:#0.##} G: {1:#0.##} B: {2:#0.##}", color.R, color.G, color.B)
    End Function
#Region "Casts to/from System.Drawing.Color"

    Public Shared Widening Operator CType(ByVal hslColor As HSLColor) As Color
        Dim b As Double = 0
        Dim r As Double = 0
        Dim g As Double = 0
        If (hslColor._luminosity <> 0) Then
            If (hslColor._saturation = 0) Then
                b = hslColor._luminosity
            Else
                Dim temp2 As Double = HSLColor.GetTemp2(hslColor)
                g = hslColor._luminosity
                r = hslColor._luminosity
                Dim temp1 As Double = ((2 * hslColor._luminosity) _
                            - temp2)
                r = HSLColor.GetColorComponent(temp1, temp2, (hslColor._hue + (1 / 3)))
                g = HSLColor.GetColorComponent(temp1, temp2, hslColor._hue)
                b = HSLColor.GetColorComponent(temp1, temp2, (hslColor._hue - (1 / 3)))
            End If

        End If

        Return Color.FromArgb(CType((255 * r), Integer), CType((255 * g), Integer), CType((255 * b), Integer))
    End Operator

    Private Shared Function GetColorComponent(ByVal temp1 As Double, ByVal temp2 As Double, ByVal temp3 As Double) As Double
        temp3 = HSLColor.MoveIntoRange(temp3)
        If (temp3 < (1 / 6)) Then
            Return (temp1 _
                        + ((temp2 - temp1) * (6 * temp3)))
        ElseIf (temp3 < 0.5) Then
            Return temp2
        ElseIf (temp3 < (2 / 3)) Then
            Return (temp1 _
                        + ((temp2 - temp1) _
                        * (((2 / 3) _
                        - temp3) _
                        * 6)))
        Else
            Return temp1
        End If

    End Function

    Private Shared Function MoveIntoRange(ByVal temp3 As Double) As Double
        If (temp3 < 0) Then
            temp3 = (temp3 + 1)
        ElseIf (temp3 > 1) Then
            temp3 = (temp3 - 1)
        End If

        Return temp3
    End Function

    Private Shared Function GetTemp2(ByVal hslColor As HSLColor) As Double
        Dim temp2 As Double
        If (hslColor._luminosity < 0.5) Then
            temp2 = (hslColor._luminosity * (1 + hslColor._saturation))
        Else
            temp2 = (hslColor._luminosity _
                        + (hslColor._saturation _
                        - (hslColor._luminosity * hslColor._saturation)))
        End If

        Return temp2
    End Function

    Public Shared Widening Operator CType(ByVal color As Color) As HSLColor
        Dim hslColor As HSLColor = New HSLColor
        hslColor._hue = (color.GetHue / 360)
        ' we store hue as 0-1 as opposed to 0-360 
        hslColor._luminosity = color.GetBrightness
        hslColor._saturation = color.GetSaturation
        Return hslColor
    End Operator
#End Region

    Public Sub SetRGB(ByVal red As Integer, ByVal green As Integer, ByVal blue As Integer)
        Dim hslColor As HSLColor = CType(Color.FromArgb(red, green, blue), HSLColor)
        Me._hue = hslColor._hue
        Me._saturation = hslColor._saturation
        Me._luminosity = hslColor._luminosity
    End Sub

    Public Sub New()
        MyBase.New

    End Sub

    Public Sub New(ByVal color As Color)
        MyBase.New
        Me.SetRGB(color.R, color.G, color.B)
    End Sub

    Public Sub New(ByVal red As Integer, ByVal green As Integer, ByVal blue As Integer)
        MyBase.New
        Me.SetRGB(red, green, blue)
    End Sub

    Public Sub New(ByVal hue As Double, ByVal saturation As Double, ByVal luminosity As Double)
        MyBase.New
        Me.Hue = hue
        Me.Saturation = saturation
        Me.Luminosity = luminosity
    End Sub
End Class