﻿Option Strict On

Public Class Tile
    Inherits Label

    Dim imageParts As New List(Of Bitmap)
    Public colorGroup As Integer

    Public xIndex As Integer
    Public yIndex As Integer

    Public isNode As Boolean = False
    Public isCompleted As Boolean = False

    Public game As GameForm

    Public Sub New(gameForm As GameForm, group As Integer, x As Integer, y As Integer)
        game = gameForm
        colorGroup = group
        xIndex = x
        yIndex = y
    End Sub

    Public Sub Reset()
        isNode = False
        colorGroup = -1
        imageParts.Clear()
    End Sub

    Sub Draw(tileSize As Size, tilePos As Point)
        Size = tileSize
        Location = tilePos
        UpdateImage()
    End Sub

    Sub UpdateImage()
        imageParts.Clear()

        ' If we are not in the solving phase, return
        If game.solver Is Nothing Then
            Return
        End If

        ' If the images aren't loaded yet or the tile is black, refresh and return
        If ImageLoader.images Is Nothing Or colorGroup = -1 Then
            Refresh()
            Return
        End If

        imageParts = ImageLoader.GetImageParts(game, Me)

        Refresh()
    End Sub

    Private Sub Tile_Paint(sender As Object, e As PaintEventArgs) Handles MyBase.Paint
        e.Graphics.FillRectangle(New SolidBrush(Color.Black), New Rectangle(New Point(0, 0), Size))

        If colorGroup = -1 Or imageParts.Count = 0 Then
            Return
        End If

        If isCompleted Then
            ' Give it a fancy background if the flow is completed
            e.Graphics.FillRectangle(New SolidBrush(ImageLoader.GetDarkColor(colorGroup)), New Rectangle(New Point(0, 0), Size))
        End If

        Dim attributes As Imaging.ImageAttributes = ImageLoader.GetImageAttributes(colorGroup)

        For Each part As Bitmap In imageParts
            DrawTileImage(e, part, attributes)
        Next
    End Sub


    Sub DrawTileImage(e As PaintEventArgs, i As Image, attributes As Imaging.ImageAttributes)
        e.Graphics.DrawImage(i, New Rectangle(New Point(0, 0), Size), 0, 0, i.Width, i.Height, GraphicsUnit.Pixel, attributes)
    End Sub

    Sub Tile_MouseDown() Handles MyBase.MouseDown
        If game.solver IsNot Nothing Then
            Capture = False
            game.solver.StartFlow(Me)
        End If
    End Sub

    Sub Tile_MouseOver() Handles MyBase.MouseEnter
        If game.solver IsNot Nothing Then
            game.solver.AddFlow(Me)
        End If
    End Sub

    Sub Tile_MouseUp() Handles MyBase.MouseUp
        If game.solver IsNot Nothing Then
            game.solver.EndFlow(Me)
        End If
    End Sub
End Class
