﻿Option Strict On

Public Class ImageLoader
    Public Shared images As New Dictionary(Of String, Bitmap)
    Shared attributes As New List(Of Imaging.ImageAttributes)

    Public Shared Function GetImage(name As String) As Bitmap
        Return images.Item(name)
    End Function

    Public Shared Sub Load()
        images.Clear()

        ' Copy resources that are an image to the images dictionary
        ' Source: https://stackoverflow.com/a/6913386
        Dim ResourceSet As Resources.ResourceSet = My.Resources.ResourceManager.GetResourceSet(Globalization.CultureInfo.CurrentCulture, True, True)
        For Each Dict As DictionaryEntry In ResourceSet.OfType(Of Object)()
            If TypeOf (Dict.Value) Is Image Then
                Dim bitmap = New Bitmap(CType(Dict.Value, Image))
                ' Make sure the transparency works correctly
                bitmap.MakeTransparent()
                images.Add(Dict.Key.ToString(), bitmap)
            End If
        Next
    End Sub

    Shared Function CreateImageAttributes(index As Integer) As Imaging.ImageAttributes
        ' Source: https://docs.microsoft.com/en-us/dotnet/framework/winforms/advanced/how-to-use-a-color-remap-table
        Dim attributes As New Imaging.ImageAttributes()
        Dim colorMap As New Imaging.ColorMap()

        colorMap.OldColor = Color.FromArgb(255, 255, 255, 255)
        colorMap.NewColor = GetColor(index)
        Dim remapTable As Imaging.ColorMap() = {colorMap}

        attributes.SetRemapTable(remapTable, Imaging.ColorAdjustType.Bitmap)
        Return attributes
    End Function

    ' Returns the correct image attributes, will automatically generate them if they don't exist yet
    Public Shared Function GetImageAttributes(index As Integer) As Imaging.ImageAttributes
        While index >= attributes.Count
            attributes.Add(CreateImageAttributes(attributes.Count))
        End While

        Return attributes(index)
    End Function

    Public Shared Function GetImageParts(game As GameForm, tile As Tile) As List(Of Bitmap)
        ' These reference loops though
        ' I mean seriously, I could keep going in loops
        ' grid.game.grid.game.grid.game.solver.flows
        ' I just wanted to say that, don't ask me why
        Dim currentFlow = game.solver.flows(tile.colorGroup)
        Dim grid = game.grid
        Dim imageParts As New List(Of Bitmap)

        If currentFlow.path.Contains(tile) Then
            Dim flowNeighbours As New List(Of Tile)

            Dim prevIndex = currentFlow.path.IndexOf(tile) - 1
            If prevIndex >= 0 Then
                flowNeighbours.Add(currentFlow.path(prevIndex))
            End If

            Dim nextIndex = currentFlow.path.IndexOf(tile) + 1
            If nextIndex < currentFlow.path.Count Then
                flowNeighbours.Add(currentFlow.path(nextIndex))
            End If

            For Each t As Tile In flowNeighbours
                If grid.TopOf(t, tile) Then
                    imageParts.Add(GetImage("Path_Up"))
                ElseIf grid.RightOf(t, tile) Then
                    imageParts.Add(GetImage("Path_Right"))
                ElseIf grid.BottomOf(t, tile) Then
                    imageParts.Add(GetImage("Path_Down"))
                ElseIf grid.LeftOf(t, tile) Then
                    imageParts.Add(GetImage("Path_Left"))
                End If
            Next
        End If

        If tile.isNode Then
            imageParts.Add(GetImage("Dot_Large"))
        ElseIf currentFlow.IsTail(tile) Then
            imageParts.Add(GetImage("Dot_Medium"))
        Else
            imageParts.Add(GetImage("Dot_Small"))
        End If

        Return imageParts
    End Function

    ' Some golden ratio sorcery!
    ' Some smart guy says multiplying with a random irrational number creates fancy distinct colors!
    ' Source: https://gamedev.stackexchange.com/a/46469

    Const GOLDEN_RATIO As Double = 0.618033988749895

    Public Shared Function GetColor(colorGroup As Integer) As Color
        Return New HSLColor(colorGroup * GOLDEN_RATIO * 255 Mod 255, 150.0, ((Math.Sqrt(1.0 - ((colorGroup + 1) * GOLDEN_RATIO) Mod 0.5) * 255) Mod 255) / 2)
    End Function

    Public Shared Function GetDarkColor(colorGroup As Integer) As Color
        Return New HSLColor(colorGroup * GOLDEN_RATIO * 255 Mod 255, 150.0, ((Math.Sqrt(1.0 - ((colorGroup + 1) * GOLDEN_RATIO) Mod 0.5) * 255) Mod 255) / 4)
    End Function
End Class
