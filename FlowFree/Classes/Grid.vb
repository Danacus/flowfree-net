﻿Option Strict On

Public Class Grid
    Inherits Panel
    Public grid(,) As Tile
    Public flows() As Flow
    Dim tileSize As Integer
    Public game As GameForm

    Public Sub New(gameForm As GameForm)
        ' Let's keep a reference to the game this grid belongs to
        game = gameForm

        ReDim grid(game.gridSize, game.gridSize)

        ' Adds tiles to the grid and adds them to the GridContainer Controls
        For i As Integer = grid.GetLowerBound(0) To grid.GetUpperBound(0)
            For j As Integer = grid.GetLowerBound(1) To grid.GetUpperBound(1)
                grid(i, j) = New Tile(game, -1, i, j)
                Controls.Add(grid(i, j))
            Next
        Next
    End Sub

    Public Sub Clear()
        For Each tile As Tile In grid
            tile.Reset()
        Next
    End Sub

    ' Tells all tiles to scale and move to their position, based on the size of the Game Form
    Sub DrawGrid()
        Dim minDimension As Integer = Math.Min(game.Size.Height, game.Size.Width)

        ' Adding 2 to gridsize magically fixes the issue with tiles being off-screen
        tileSize = CInt(minDimension / (game.gridSize + 2))

        CenterContainer()

        For i As Integer = grid.GetLowerBound(0) To grid.GetUpperBound(0)
            For j As Integer = grid.GetLowerBound(1) To grid.GetUpperBound(1)
                grid(i, j).Draw(
                    New Size(tileSize, tileSize),
                    New Point(i * tileSize, j * tileSize)
                )
            Next
        Next
    End Sub

    ' Centers the GridContainer
    Sub CenterContainer()
        Dim minDimension = Math.Min(game.Size.Height, game.Size.Width) - 10

        Height = minDimension
        Width = minDimension

        Location = New Point(
            CInt(game.Size.Width / 2 - Width / 2),
            CInt(game.Size.Height / 2 - Height / 2)
        )
    End Sub

    Public Function GetNeighbours(tile As Tile) As List(Of Tile)
        Dim up = tile.yIndex - 1
        Dim down = tile.yIndex + 1
        Dim left = tile.xIndex - 1
        Dim right = tile.xIndex + 1

        Dim neighbours As New List(Of Tile)

        ' We'll check each side and make sure it's not off the grid
        ' Than we can add the neighbours to a list

        If up >= 0 Then
            neighbours.Add(grid(tile.xIndex, up))
        End If

        If down < grid.GetLength(1) Then
            neighbours.Add(grid(tile.xIndex, down))
        End If

        If left >= 0 Then
            neighbours.Add(grid(left, tile.yIndex))
        End If

        If right < grid.GetLength(0) Then
            neighbours.Add(grid(right, tile.yIndex))
        End If

        Return neighbours
    End Function

    ' Geo worlds!
    ' I'm sorry, it reminded me of Logica voor Informatici

    Function TopOf(tile As Tile, other As Tile) As Boolean
        Return tile.yIndex < other.yIndex
    End Function

    Function BottomOf(tile As Tile, other As Tile) As Boolean
        Return tile.yIndex > other.yIndex
    End Function

    Function LeftOf(tile As Tile, other As Tile) As Boolean
        Return tile.xIndex < other.xIndex
    End Function

    Function RightOf(tile As Tile, other As Tile) As Boolean
        Return tile.xIndex > other.xIndex
    End Function

    Function CheckGridCompleted() As Boolean
        ' Is there a tile that isn't colored?
        For Each tile As Tile In grid
            If tile.colorGroup = -1 Then
                Return False
            End If
        Next

        ' Is there a flow that isn't completed?
        For Each flow As Flow In flows
            If Not flow.IsComplete() Then
                Return False
            End If
        Next

        Return True
    End Function

    Sub CleanupFlows()
        ' Remove all tiles from the flow, except for the tail and the head
        For Each f As Flow In flows
            ' First we make the tiles that are not a tail and not a head black again
            ' We will also mark the head and tail tiles by setting isEndTile to True
            ' We cannot remove them from the list within this loop, since that would shift the indexes
            ' and cause all sorts of trouble
            For Each tile In f.path
                If Not tile.isNode Then
                    tile.colorGroup = -1
                End If
                tile.UpdateImage()
            Next

            ' Now we can safely remove them from the list
            ' f.tiles.RemoveAll(Function(t) t.colorGroup = -1)
            ' We could also just clear all flows and re-add tiles when solving the puzzle
            ' We can still tell which color belongs to which flow, since the index of a flow is equal to the color group
            f.path.Clear()
        Next
    End Sub
End Class
