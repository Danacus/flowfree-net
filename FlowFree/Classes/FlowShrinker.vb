﻿Option Strict On

Public Class FlowShrinker
    Implements ILevelGenerator

    Public _name As String = "FlowShrinker (faster, but less diverse results)"

    Private Property name As String Implements ILevelGenerator.name
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    ' Source of the algorithm: https://stackoverflow.com/a/12926928
    Sub GenerateLevel(grid As Grid, worker As System.ComponentModel.BackgroundWorker) Implements ILevelGenerator.GenerateLevel
        ' The number of flows is equal to the number of rows / columns
        ReDim grid.flows(grid.grid.GetLength(0) - 1)

        ' A shorthand for grid.flows
        Dim flows = grid.flows

        ' So I don't have to write grid.grid over and over again
        Dim g = grid.grid

        ' Part 1
        For i As Integer = g.GetLowerBound(0) To g.GetUpperBound(0)
            ' Create a flow for each column
            Dim f = New Flow

            For j As Integer = g.GetLowerBound(1) To g.GetUpperBound(1)
                ' And fill it with tiles
                Dim tile = g(i, j)
                tile.colorGroup = i
                f.path.Add(tile)
            Next

            ' Store it in the array located in the Grid class
            flows(i) = f
        Next

        ' Part 2
        Dim random As Random = New Random()

        ' We could 3 as the minimum length of a flow
        ' even though it would be valid to connect 2 adjacent nodes.
        ' This could make the levels more interesting on larger grids,
        ' but it doesn't work out very well on grids smaller than 6x6
        ' For that reason I'm setting the minimum length to 2 
        ' EDIT: maybe I should make the minimum length depend on the size of the grid
        Dim minimumLength = 2

        If g.GetLength(0) > 6 Then
            minimumLength = 3
        End If

        If g.GetLength(0) > 10 Then
            minimumLength = 4
        End If

        For i As Integer = 0 To CInt(Math.Pow(2, 18))
            ' Pick a random flow
            Dim num As Integer = random.Next(0, flows.GetLength(0) - 1)
            Dim f As Flow = flows(num)


            ' Here I'm trying to reduce the amount of calls to the GameForm
            If (i Mod 50 = 0) Then
                worker.ReportProgress(i)
            End If

            If worker.CancellationPending Then
                Return
            End If

            ' We will run the next part twice
            ' Once for the head, and once for the tail
            For j As Integer = 0 To 1

                If f.path.Count <= minimumLength Then
                    Continue For
                End If

                ' Use the head as default
                Dim tile = f.path(0)

                'Change it to the tail if we're in the second iteration
                If j = 1 Then
                    tile = f.path(f.path.Count - 1)
                End If

                ' Get the neighbours of the current head or tail, but
                ' only keep the neighbours that are a head or a tail of their flow
                Dim neighbours = grid.GetNeighbours(tile).Where(Function(t) flows(t.colorGroup).IsHead(t) Or flows(t.colorGroup).IsTail(t))

                If Not neighbours.Count = 0 Then
                    ' Pick a random neighbour
                    Dim nextTile = neighbours(random.Next(0, neighbours.Count - 1))

                    ' We know that the index of the flow of nextTile is equal to it's color group number
                    ' since we assigned colorGroup to i and the flow f to flows(i)
                    Dim nextFlow = flows(nextTile.colorGroup)

                    ' Remove the head or tail from the flow, the next element will become the new head
                    f.path.Remove(tile)

                    ' Change the group of the original tile
                    tile.colorGroup = nextTile.colorGroup

                    ' When it's a head ...
                    If nextFlow.IsHead(nextTile) Then
                        ' Insert the original tile at the beginning, this will become the new head
                        nextFlow.path.Insert(0, tile)
                    Else ' Otherwise it must be a tail, since all of our neighbours in the list are either head or tail
                        ' Add the original tile to the end of the list, this will become the new tail
                        nextFlow.path.Add(tile)
                    End If

                    ' At the end of an iteration, one flow will be 1 unit shorter,
                    ' while another flow will be 1 unit longer.
                    ' This way the board will always be valid
                End If
            Next
        Next

        For Each f As Flow In flows
            f.GetHead().isNode = True
            f.GetTail().isNode = True
        Next
    End Sub
End Class
