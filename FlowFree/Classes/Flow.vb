﻿Option Strict On

Public Class Flow

    ' Stores an ordered list of all the tiles in the flow
    Public path As New List(Of Tile)

    ' Is it a head (the first tile in the path)?
    Function IsHead(tile As Tile) As Boolean
        Return path.IndexOf(tile) = 0
    End Function

    ' Is it a tail (the last tile in the path)?
    Function IsTail(tile As Tile) As Boolean
        Return path.IndexOf(tile) = path.Count - 1
    End Function

    Function GetHead() As Tile
        Return path(0)
    End Function

    Function GetTail() As Tile
        Return path(path.Count - 1)
    End Function

    ' Is the flow complete (does the path contain 2 nodes)
    Function IsComplete() As Boolean
        If path.Count < 2 Then
            Return False
        End If

        Return GetHead().isNode And GetTail().isNode
    End Function

    ' Adds a tile to the path
    Sub AddTile(tile As Tile)
        Dim prev = GetTail()
        path.Add(tile)
        tile.colorGroup = GetHead().colorGroup
        prev.UpdateImage()
        tile.UpdateImage()
    End Sub

    Sub SetTilesComplete(complete As Boolean)
        For i As Integer = 0 To path.Count - 1
            path(i).isCompleted = complete
            path(i).UpdateImage()
        Next
    End Sub

    ' Shrinks the flow to a given index (every tile after this index will be removed)
    Sub Shrink(index As Integer)
        Dim tail = GetTail()

        While path.Count - 1 > index And path.Count > 0
            If Not GetTail().isNode Then
                GetTail().colorGroup = -1
            End If

            GetTail().isCompleted = False
            GetTail().UpdateImage()
            path.RemoveAt(path.Count - 1)
        End While

        tail.UpdateImage()

        If path.Count > 0 Then
            ' Update the current tail, but also the original one, as it might be a node that needs to be updated
            GetTail().UpdateImage()
        End If
    End Sub
End Class
