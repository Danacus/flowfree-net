﻿Option Strict On

Public Class Solver
    Dim game As GameForm
    Dim grid As Grid
    Public enabled As Boolean = True
    Public flows() As Flow
    Dim currentFlow As Flow

    Public Sub New(gameForm As GameForm)
        game = gameForm
        grid = game.grid
        flows = grid.flows
    End Sub

    Public Sub StartFlow(tile As Tile)
        If Not enabled Then
            Return
        End If

        ' If we try to start a flow on a black tile, ignore it
        If tile.colorGroup = -1 Then
            Return
        End If

        currentFlow = flows(tile.colorGroup)

        If currentFlow.path.Count = 0 Then
            ' If the path is empty, we can add the current tile to the path
            currentFlow.path.Add(tile)

            ' If not, we'll see if it might be a node
        ElseIf tile.isNode Then
            ' In that case we can clear the path and create a new one (we migth be at the other end, while a path exists that starts from the first end)
            ' We will use Flow.Shrink with a negative index for that, as it makes sure that the tiles become black again before getting removed
            currentFlow.Shrink(-1)
            currentFlow.path.Add(tile)
            tile.UpdateImage()

            ' Otherwise, we need to verify that the tile is in the path
        ElseIf currentFlow.path.Contains(tile) Then
            ' If it's in the path, we can shrink the path to that tile
            currentFlow.Shrink(currentFlow.path.IndexOf(tile))
        Else
            ' This block should never be reached, but I'll put it here just to be safe
            currentFlow = Nothing
        End If
    End Sub

    Public Sub AddFlow(tile As Tile)
        ' If there is no flow (meaning we're not holding the mouse button), ignore it and return
        If currentFlow Is Nothing Then
            Return
        End If

        ' If the flow is complete and we are on a black tile, don't add any new tiles
        If currentFlow.IsComplete() And tile.colorGroup = -1 Then
            Return
        End If

        If currentFlow.path.Contains(tile) Then
            ' If it's in the path, shrink the path back to that tile
            currentFlow.Shrink(currentFlow.path.IndexOf(tile))
        ElseIf grid.GetNeighbours(tile).Contains(currentFlow.GetTail()) And (tile.colorGroup = -1 Or tile.colorGroup = currentFlow.GetHead().colorGroup) Then
            ' If tile has a neighbour in the current flow
            currentFlow.AddTile(tile)
        End If
    End Sub

    Public Sub EndFlow(tile As Tile)
        ' If there is no current flow, return
        If currentFlow Is Nothing Then
            Return
        End If

        If currentFlow.IsComplete() Then
            currentFlow.SetTilesComplete(True)
        Else
            currentFlow.SetTilesComplete(False)
        End If

        currentFlow = Nothing

        ' If the grid is completed, end the game
        If grid.CheckGridCompleted() Then
            game.EndGame()
        End If
    End Sub
End Class
