﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GameForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GameForm))
        Me.currentTimeLabel = New System.Windows.Forms.Label()
        Me.BestTimeLabel = New System.Windows.Forms.Label()
        Me.UITimer = New System.Windows.Forms.Timer(Me.components)
        Me.LevelGenWorker = New System.ComponentModel.BackgroundWorker()
        Me.LevelGenLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'currentTimeLabel
        '
        Me.currentTimeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.currentTimeLabel.AutoSize = True
        Me.currentTimeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.currentTimeLabel.ForeColor = System.Drawing.Color.White
        Me.currentTimeLabel.Location = New System.Drawing.Point(590, 9)
        Me.currentTimeLabel.Name = "currentTimeLabel"
        Me.currentTimeLabel.Size = New System.Drawing.Size(0, 20)
        Me.currentTimeLabel.TabIndex = 0
        '
        'BestTimeLabel
        '
        Me.BestTimeLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BestTimeLabel.AutoSize = True
        Me.BestTimeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BestTimeLabel.ForeColor = System.Drawing.Color.White
        Me.BestTimeLabel.Location = New System.Drawing.Point(617, 29)
        Me.BestTimeLabel.Name = "BestTimeLabel"
        Me.BestTimeLabel.Size = New System.Drawing.Size(0, 16)
        Me.BestTimeLabel.TabIndex = 1
        '
        'UITimer
        '
        Me.UITimer.Enabled = True
        Me.UITimer.Interval = 10
        '
        'LevelGenWorker
        '
        Me.LevelGenWorker.WorkerReportsProgress = True
        Me.LevelGenWorker.WorkerSupportsCancellation = True
        '
        'LevelGenLabel
        '
        Me.LevelGenLabel.AutoSize = True
        Me.LevelGenLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LevelGenLabel.ForeColor = System.Drawing.Color.White
        Me.LevelGenLabel.Location = New System.Drawing.Point(12, 9)
        Me.LevelGenLabel.Name = "LevelGenLabel"
        Me.LevelGenLabel.Size = New System.Drawing.Size(189, 16)
        Me.LevelGenLabel.TabIndex = 2
        Me.LevelGenLabel.Text = "Generating Level... Iterations: 0"
        '
        'GameForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(768, 622)
        Me.Controls.Add(Me.LevelGenLabel)
        Me.Controls.Add(Me.BestTimeLabel)
        Me.Controls.Add(Me.currentTimeLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "GameForm"
        Me.Text = "FlowFree.NET - New Game"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents currentTimeLabel As Label
    Friend WithEvents BestTimeLabel As Label
    Friend WithEvents UITimer As Timer
    Friend WithEvents LevelGenWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents LevelGenLabel As Label
End Class
